<?php
class Model
{
    public function call_db($query){
        $mysqli = Db_connect::connect();
        return mysqli_query($mysqli, $query);
    }

    public function get_data($table)
    {
        $query = 'SELECT * from '.$table;
        return $this->call_db($query);
    }

    public function get_raw_by_id($id, $table)
    {
        $query = 'SELECT * from `'.$table.'` WHERE id='.$id;
        return mysqli_fetch_row($this->call_db($query));
    }


    public function delete_raw($id, $table)
    {
        $query = 'DELETE FROM `'.$table.'` WHERE `'.$table.'`.`id` = '.$id;
        $this->call_db($query);
    }
}

