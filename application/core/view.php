<?php
class View
{
    //public $template_view; // здесь можно указать общий вид по умолчанию.

    function generate($content_view, $template_view, $data = null, $message=null)
    {
        include 'application/views/'.$template_view;
        include 'application/views/error_popup_view.php';
    }
}

