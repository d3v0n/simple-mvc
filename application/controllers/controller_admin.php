<?php
include_once 'application/models/model_user.php';
include_once 'application/models/model_portfolio.php';
include_once 'application/models/model_news.php';


class Controller_Admin extends Controller
{
    public Model_Portfolio $model_portfolio;
    public Model_News $model_news;
    public Model_User $model_user;
    public string $upl_dir = 'images/';
    public array $allowedfileExtensions = array('jpg',  'png',);

    function __construct()
    {
        if (str_contains($_SERVER['REQUEST_URI'], 'admin') && (!isset($_SESSION["is_admin"]) || $_SESSION["is_admin"] != 1)) {
            Route::ErrorPage404();
        }
//        $this->model = new Model();
        $this->model_user = new Model_User();
        $this->model_portfolio = new Model_Portfolio();
        $this->model_news = new Model_News();
        $this->view = new View();

    }

    function action_index()
    {
        $data['portfolio'] = $this->model_portfolio->get_data('portfolio');
        $data['news'] = $this->model_news->get_data('news');
        $this->view->generate('admin_view.php', 'template_view.php', $data);
    }

    function action_create()
    {
        switch ($_POST['type']) {
            case 'news':
                var_dump($_POST);
                $raw_data = array(
                    'title' => $_POST['title'],
                    'body' => $_POST['body'],
                );
                $this->model_news->add_raw($raw_data);
                break;
            case 'portfolio':
                $raw_data = array(
                    'year' => $_POST['year'],
                    'site' => $_POST['site'],
                    'description' => $_POST['description'],
                );
                $this->model_portfolio->add_raw($raw_data);
                break;
        }
        $this->action_index();
    }

    function action_edit()
    {
        switch ($_POST['type']) {
            case 'portfolio':
                $data = $this->model_portfolio->get_raw_by_id($_POST['id'], 'portfolio');
                $this->view->generate('edit_portfolio_view.php', 'template_view.php', $data);
                break;
            case 'news':
                $data = $this->model_news->get_raw_by_id($_POST['id'], 'news');
                $this->view->generate('edit_news_view.php', 'template_view.php', $data);
                break;
            case 'user':
                $data = mysqli_fetch_row($this->model_user->get_user($_POST['username']));
                if (!$data) {
                    Route::ErrorPage404();
                }
                var_dump($data);
                $this->view->generate('edit_user_view.php', 'template_view.php', $data);
                break;
        }

    }

    function action_save()
    {
        switch ($_POST['type']) {
            case 'portfolio':

                $raw_data = array(
                    'id' => $_POST['id'],
                    'year' => $_POST['year'],
                    'site' => $_POST['site'],
                    'description' => $_POST['description'],
                );
                $this->model_portfolio->update_raw($raw_data);
                break;
            case 'news':
                $raw_data = array(
                    'id' => $_POST['id'],
                    'title' => $_POST['title'],
                    'body' => $_POST['body'],
                    'image' => $this->save_file($_FILES)
                );;
                $this->model_news->update_raw($raw_data);
                break;
            case 'user':
                $raw_data = array(
                    'id' => $_POST['user_id'],
                    'is_disabled' => isset($_POST['is_disabled']) ? 1 : 'NULL',
                );
                $this->model_user->save_user($raw_data['id'], $raw_data['is_disabled']);
                break;
        }

        $this->action_index();
    }

    function action_delete()
    {
        switch ($_POST['type']) {
            case 'portfolio':
                $this->model_portfolio->delete_raw($_POST['id'], 'portfolio');
                $this->action_index();
                break;
            case 'news':
                $this->model_news->delete_raw($_POST['id'], 'news');
                $this->action_index();
                break;
        }
    }

    function save_file($FILES)
    {
        $file_name = $FILES['user_file']['name'];
        $file_name_arr = explode('.', $file_name);
        $file_ext =  strtolower(end($file_name_arr));
        $file = array(
            'f_temp_path' => $FILES['user_file']['tmp_name'],
            'f_name' => $file_name,
            'f_size' => $FILES['user_file']['size'],
            'f_type' => $FILES['user_file']['type'],
            'f_name_arr' => $file_name_arr,
            'file_ext' => $file_ext,
        );
        $new_file_name = uniqid().'.'.$file['file_ext'];
        $destination = $this->upl_dir.$new_file_name;
        if(in_array($file['file_ext'], $this->allowedfileExtensions)){
            move_uploaded_file($FILES["user_file"]["tmp_name"], $destination);
            return $destination;
        }
//        $upl_file = $this->upl_dir .
//            var_dump(uniqid());

    }

}