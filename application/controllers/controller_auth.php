<?php
include 'application/models/model_user.php';

class Controller_Auth extends Controller
{
    public string $pass_pattern = '/^[a-zA-Z0-9]{8,30}$/'; // start at start, a-z, A-Z, 0-9, 8-30 characters, end of string;
    public string $login_pattern = '/^[a-zA-Z0-9]{1,20}$/';

    function __construct()
    {
        $this->model = new Model_User();
        $this->view = new View();
    }

    function action_index()
    {
        $this->view->generate('auth_view.php', 'template_view.php');
    }

    function action_login()
    {
        $messaage = '';
        if (isset($_POST['login']) && isset($_POST['password'])) {
            if (preg_match($this->login_pattern, $_POST['login']) && preg_match($this->pass_pattern, $_POST['password']))
                $login_data = array(
                    'login' => $_POST['login'],
                    'pass' => $_POST['password']
                );
            else {
                $messaage = 'Wrong login or password';
                $this->view->generate('auth_view.php', 'template_view.php', null, $messaage);
                return false;
            }
        }
        $user = mysqli_fetch_row($this->model->get_user($login_data['login']));
        if ($user && !($user[5])) {
            if (password_verify($login_data['pass'], $user[2])) {
                $_SESSION["is_auth"] = true; //Делаем пользователя авторизованным
                $_SESSION["login"] = $_POST['login']; //Записываем в сессию логин пользователя
                $_SESSION["is_admin"] = $user[4];
                $_SESSION['is_disabled'] = $user[5];
                $this->view->generate('main_view.php', 'template_view.php');
                var_dump($_SESSION['is_disabled']);
            }
            else {
                    $messaage = 'Wrong login or password';
                    $this->view->generate('auth_view.php', 'template_view.php', null, $messaage);
                    return false;
                }
        } else {
            $messaage = 'Wrong login or password';
            $this->view->generate('auth_view.php', 'template_view.php', null, $messaage);
            return false;
        }


//        $query_user = $this->model->login($_POST['login'], $_POST['password']);
//        if (mysqli_num_rows($query_user)) {
//            $_SESSION["is_auth"] = true; //Делаем пользователя авторизованным
//            $_SESSION["login"] = $_POST['login']; //Записываем в сессию логин пользователя
//            $_SESSION["is_admin"] = mysqli_fetch_row($query_user)[4];
//            $this->view->generate('main_view.php', 'template_view.php');
//        } else {
//            $messaage = 'Wrong login or password';
//            $this->view->generate('auth_view.php', 'template_view.php', null, $messaage);
//
//        }
    }


    function action_register()
    {
        $message = '';
        if (isset($_POST['login']) && isset($_POST['password1']) && isset($_POST['password2'])) {
            $register_data = array(
                'login' => $_POST['login'],
                'p1' => $_POST['password1'],
                'p2' => $_POST['password2']
            );
        } else {
            $message .= 'Something goes wrong, try again.';
            $this->view->generate('auth_view.php', 'template_view.php', null, $message);
            return false;
        }
        if (!preg_match($this->login_pattern, $register_data['login'])) {
            $message .= 'Login must contain only a letters and digits with maximum length of 20.';
            $this->view->generate('auth_view.php', 'template_view.php', null, $message);
            return false;
        }
        if ($register_data['p1'] != $register_data['p2']) {
            $message .= 'Passwords must be same.';
            $this->view->generate('auth_view.php', 'template_view.php', null, $message);
            return false;
        }
        if (!preg_match($this->pass_pattern, $register_data['p1'])) {
            $message .= 'Password must contain only a letters and digits with maximum length of 30.';
            $this->view->generate('auth_view.php', 'template_view.php', null, $message);
            return false;
        }
        if (mysqli_num_rows($this->model->get_user($register_data['login']))) {
            $message = 'Username already taken';
            $this->view->generate('auth_view.php', 'template_view.php', null, $message);
            return false;
        }
        $hashed_password = password_hash($register_data['p1'], PASSWORD_DEFAULT);
        $this->model->create_user($register_data['login'], $hashed_password);
        $message = 'Registration successful';
        $this->view->generate('auth_view.php', 'template_view.php', null, $message);
    }

    function action_logout()
    {
        $_SESSION = array();
        session_unset();
        $this->view->generate('main_view.php', 'template_view.php');
    }
}