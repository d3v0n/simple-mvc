<?php
include 'application/models/model_user.php';

class Controller_News extends Controller
{
    function __construct()
    {
        $this->model = new Model_News();
        $this->model_user = new Model_User();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data('news');
//        var_dump($data);
        $this->view->generate('news_view.php', 'template_view.php', $data);
    }

    function action_read()
    {
        $data = $this->model->get_raw_by_id($_GET['id'], 'news');
        $data[4] = $this->model->get_commentaries_by_post_id($_GET['id']);
        $this->view->generate('news_post_view.php', 'template_view.php', $data);
    }

    function action_add_comment()
    {
        if(!$_SESSION['is_auth'] || !($_SESSION['is_disabled'])){
            $this->action_index();
            return;
        }
        $user = mysqli_fetch_row($this->model_user->get_user($_SESSION['login']));
        $data = array(
            'post_id' => $_POST['news_id'],
            'user_id' => $user[0],
            'body' => $_POST['comment_body'],
        );
        $this->model->add_comment($data);
        $data = $this->model->get_raw_by_id($_POST['news_id'], 'news');
        $data[3] = $this->model->get_commentaries_by_post_id($_POST['news_id']);
        $this->view->generate('news_post_view.php', 'template_view.php', $data);
    }

    function action_delete_comm()
    {
        var_dump($_GET);
        $author = $this->model->get_info_by_comment_id($_GET['id']);
        if ($author[3] == $_SESSION['login'] || $_SESSION['is_admin']) {
            $this->model->hide_comment($_GET['id']);
        }
        $data = $this->model->get_raw_by_id($author[1], 'news');
        $data[3] = $this->model->get_commentaries_by_post_id($author[1]);
        $this->view->generate('news_post_view.php', 'template_view.php', $data);

    }
}
