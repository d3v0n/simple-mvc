<div>
    <h1><?php echo $data[1]?></h1>
    <p><?php echo $data[2]?></p>
    <img src="/<?php echo $data[3]?>">
</div>
<div id="comments">
    <?php
    foreach ($data[4] as $coms){
        if($coms['is_hidden']) {continue;} else {
            echo '
            <div class="comment">
            <h3>' . $coms['username'] . '</h3>
            <p>' . $coms['body'] . '</p>';
            if(isset($_SESSION['login'])){
            if ($_SESSION['login'] == $coms['username'] || $_SESSION['is_admin']) {
                echo '<a href="/news/delete_comm/?id=' . $coms['id'] . '">Delete</a>';
            }}
            echo '</div>';
        }
    }
    if(isset($_SESSION['is_auth'])){
        echo '
            <form method="post" action="/news/add_comment">
               <input type="hidden" name="news_id" value="'.$data[0].'">
               <input type="text" name="comment_body">
               <input type="submit" value="Send">
            </form>';
    }
    ?>
</div>