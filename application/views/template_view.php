<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <style type="text/css">
        <?php
            include 'css/style.css';
        ?>
    </style>
</head>
<body>
<?php include 'application/views/'.$content_view; ?>
</body>
</html>
