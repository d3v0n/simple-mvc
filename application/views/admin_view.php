<h1>Админ-панель</h1>
<h2>Редактирование новостей</h2>
<form method="post" action="/admin/create">
    <input type="hidden" name="type" value="news">
    <input type="text" name="title" placeholder="Title">
    <input type="text" name="body" placeholder="Body">
    <input type="submit" value="Add">
</form>
<?php
foreach ($data['news'] as $row) {
    echo '<div>
            <h3>'.$row['title'].'</h3>
            <p>'.$row['body'].'</p>
            <form method="post" action="/admin/edit">
                <input type="hidden" name="type" value="news">
                <input type="submit" value="Edit">
                <input type="hidden" name="id" value="'.$row['id'].'">
            </form>
            <form method="post" action="/admin/delete" method="post">
                <input type="hidden" name="type" value="news">
                <input type="submit" value="Delete">
                <input type="hidden" name="id" value="'.$row['id'].'">
            </form>
          </div>';
}
?>
<h2>Редактирование юзера</h2>
<form method="post" action="/admin/edit" id="user">
    <input type="hidden" name="type" value="user">
    <input type="text" name="username" placeholder="Username">
    <input type="submit" value="Edit">
</form>

<h2>Редактирование портфолио</h2>
<p>
<form method="post" action="/admin/create" id="create">
    <input type="hidden" name="type" value="portfolio">
    <input type="number" placeholder="Year" name="year">
    <input type="text" maxlength="50" name="site" placeholder="Site">
    <input type="text" maxlength="250" name="description" placeholder="Description">
    <input type="submit" value="Add">
</form>
<table>
    <tr>
        <td>id</td>
        <td>Год</td>
        <td>Проект</td>
        <td>Описание</td>
        <td>Edit</td>
    </tr>


    <?php

    foreach ($data['portfolio'] as $row) {
        echo '<tr><td>' . $row['id'] . '</td><td>' . $row['year'] . '</td><td>' . $row['site'] . '</td><td>' . $row['description'] .
            '</td><td>
            <form method="post" action="/admin/edit">
                <input type="hidden" name="type" value="portfolio">
                <input type="submit" value="Edit">
                <input type="hidden" name="id" value="'.$row['id'].'">
            </form>
            <form method="post" action="/admin/delete" method="post">
                <input type="hidden" name="type" value="portfolio">
                <input type="submit" value="Delete">
                <input type="hidden" name="id" value="'.$row['id'].'">
            </form>

            </td></tr>';
    }
    ?>
</table>
</p>

