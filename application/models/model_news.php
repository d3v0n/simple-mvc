<?php

class Model_News extends Model
{
    public function add_raw($raw_data)
    {
        $query = 'INSERT INTO `news` (`id`, `title`, `body`) VALUES (NULL, \'' . $raw_data['title'] . '\', \'' . $raw_data['body'] . '\');';
        $this->call_db($query);
    }

    public function update_raw($raw_data)
    {
        $query = 'UPDATE `news` SET `title` = \'' . $raw_data["title"] . '\', `body` = \'' . $raw_data["body"] . '\', `image` = \''.$raw_data['image'].'\' WHERE `news`.`id` = ' . $raw_data["id"];
        $this->call_db($query);
    }

    public function get_commentaries_by_post_id($raw_data)
    {
        $query = 'SELECT c.*, u.id AS id2, u.username FROM users AS u RIGHT JOIN commentaries AS c ON u.id = c.author_id WHERE post_id = ' . $raw_data;
        return $this->call_db($query);
    }

    public function get_info_by_comment_id($id)
    {
        $query = 'SELECT c.id,  c.post_id, c.author_id, u.id AS id2, u.username FROM users AS u RIGHT JOIN commentaries AS c ON u.id = c.author_id WHERE c.id = ' . $id;
        return mysqli_fetch_row($this->call_db($query));
    }

    public function hide_comment($id)
    {
        $query = 'UPDATE `commentaries` SET `is_hidden` = 1 WHERE `commentaries`.`id` = ' . $id;
        return $this->call_db($query);
    }

    public function add_comment($data)
    {
        $query = 'INSERT INTO `commentaries` (`id`, `post_id`, `author_id`, `is_hidden`, `created_at`, `body`) VALUES (NULL, \''.$data['post_id'].'.\', \''.$data['user_id'].'\', NULL, current_timestamp(), \''.$data['body'].'\')';
        $this->call_db($query);
    }
}
